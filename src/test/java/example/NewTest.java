package example;		

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.Reporter;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;		
import org.testng.annotations.Test;	
import org.testng.annotations.BeforeTest;	
import org.testng.annotations.AfterTest;		
public class NewTest {		
	    private WebDriver driver;		
		@Test				
		public void testEasy() {	
			Reporter.log("Test case testEasy Starts Now");
			System.out.println("Starting Test");
			driver.get("https://arabia.starzplay.com/");  
			String title = driver.getTitle();	
			if(title.equals("STARZ PLAY | Watch Featured Movies and Original Series"))
			{
				Assert.assertEquals(title, "STARZ PLAY | Watch Featured Movies and Original Series");
				Reporter.log("Step 1. Verify the title of Page : Successfully verified the Title Of the Page And It is"+title);
			}else
			{
				Assert.assertEquals(title, "STARZ PLAY | Watch Featured Movies and Original Series");
				Reporter.log("Step 1. Verify the title of Page : Title Verification Failed And the it is "+title);
			}
			
			WebElement lblStart = driver.findElement(By.xpath(".//*[text()='START YOUR 30 DAYS FREE TRIAL']"));
			WebElement btnCreateNew = driver.findElement(By.xpath(".//*[@id='signupLanding']"));
			WebElement btnExistingUser = driver.findElement(By.xpath(".//*[@id='login']"));
			
			if(lblStart.isDisplayed())
			{
				Reporter.log("Step 2. Verify the Label : START YOUR 30 DAYS FREE TRIAL");
			}else
			{
				Assert.fail("Label Verification failed for the label :START YOUR 30 DAYS FREE TRIAL");
				Reporter.log("Step 2. Verify the Label : START YOUR 30 DAYS FREE TRIAL");
			}
			
			if(btnCreateNew.isDisplayed())
			{
				Reporter.log("Step 3. Verify the Button : CREATE A NEW ACCOUNT : Successfully verified");		
			}else
			{
				Assert.fail("Button Verification failed for the Button :CREATE A NEW ACCOUNT");
				Reporter.log("Step 3. Verify the Button : Verification Failed");
			}
			
			if(btnExistingUser.isDisplayed())
			{
				Reporter.log("Step 3. Verify the Button : EXISTING USER : Successfully verified");
			}else
			{
				Assert.fail("BUtton Verification failed for the Button :EXISTING USER");
				Reporter.log("Step 3. Verify the Button EXISTING USER : Verification Failed");
			}
			
			
			
			System.out.println("Getting Title");
			System.out.println(title);
			AssertJUnit.assertTrue(title.contains("S")); 		
		}	
		@BeforeTest
		public void beforeTest() {	
			System.out.println("This is setup");
			System. setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Resources/chromedriver");
		    driver = new FirefoxDriver();   
		}		
		@AfterTest
		public void afterTest() {
			driver.quit();			
		}		
}	